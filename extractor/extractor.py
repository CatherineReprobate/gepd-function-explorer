import sys, lib.HtmlGenerator, lib.Parser, lib.Rom, json, base64

if len(sys.argv) == 1:
    print("Usage: python extractor.py <rom-filename>")
    exit(1)

"""
Step 1: The Rom library reads the functions out of a ROM and returns them as an
array of dicts. Each dict contains a stage_id, function_id (eg. 0x0401) and a
'raw' element which is the entire function as a block of bytes.
"""
rom = lib.Rom.load(sys.argv[1])
functions = rom.getFunctions()

for function in functions:
    function['raw'] = base64.encodestring(function['raw']).decode('utf-8').replace('\n', '')

"""
Step 2: The parser breaks the function down into instructions and adds
descriptions for each one.
"""
parser = lib.Parser.load(rom.getGame())
for function in functions:
    parser.setFunction(function)
    function['instructions'] = parser.parse()

"""
Step 3: The generator combines the instructions with the user-added annotations
and writes the markdown files.
"""
generator = lib.HtmlGenerator.load(rom.getGame())
generator.setFunctions(functions)
generator.setAnnotationsDirectory('annotations')
generator.setOutputDirectory('public')
generator.generate()

generator.generateIndexFile()
