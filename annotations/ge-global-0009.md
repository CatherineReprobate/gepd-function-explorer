Activate alarm if seen

Wait until not moving

If Bond is in sight:

* Jog to $self->pad
* Wait until not moving
* If at pad, activate object there
* Jog to bond
