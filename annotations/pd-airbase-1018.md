Safe switch

Wait for switch activation
Enable lift/moving prop
Wait for switch to finish moving
Disable lift/moving prop
Wait for switch activation again
Play sound
Open safe door