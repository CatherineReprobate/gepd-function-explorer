PA pillar 1

Watch for one of 3 objects (some kind of glass) being destroyed.
When any is destroyed, destroy all three objects, blow up a remote mine, and set flag 21.