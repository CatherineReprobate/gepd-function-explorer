Show objectives failed message

Wait until an objective has failed

Start counting frames

After 30 seconds:

* If no objectives are failed any more, exit
* If there are still failed objectives, show messages at the 30s, 1 minute, 2 minute and 5 minute marks

Seems like if you fail an objective and then unfail it, the function quits. So if you then fail an objective again, it'll never show the message.
