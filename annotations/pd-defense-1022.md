Skedar ship

If A/SA:
- Wait for rcp120 objective complete

If PA:
- Wait for safe objective complete

Wait until player not in helipad area
Show shuttle
Show Mr Blonde guards
Wait for flag 24 or flag 25

Flag 24 (timer expired):
Destroy skedar ship
Explosions around Joanna
Wait 1 second
Explosions around Velvet
Explosions around Counter-op
Wait 1.5 seconds

Flag 25:
Stop countdown timer
Ship does animation
Play sound effect
Wait 1 second
Play sound effect
Wait 3.33 seconds
Set flag 15
Wait 4 seconds
Destroy ship