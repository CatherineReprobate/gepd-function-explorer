Purple hover bot

If Agent, move bot to pad BE and assign function 0413 (laser path).

For SA/PA:
Wait until bot activated with no program, or objective complete (active with maintenance program), or active with routine cycle

Activated with no program (0A):
- Path 08 at slow speed
- Check for deactivation or programmed with routine cleaning cycle

Routine cleaning cycle (08):
- Path 07 at medium speed

Maintenance cycle (0C):
- Path 09 at high speed (takes bot to lasers)
- When path ended assign 0413 (laser cycle)