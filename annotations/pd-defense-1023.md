Skedar shuttle expired

If A/SA:
- Wait for rcp120 objective complete

If PA:
- Wait for safe objective complete

Wait until skedar shuttle destroyed
Wait 0.5 seconds
If flag 15, exit
Show "The bomb has been detonated"
Set flag 24
Explosions around Joanna
Wait 1 second
Explosions around Velvet
Explosions around counter op
Wait 1.5 seconds
End level