Update flags 7 and 25

Flag 7 = !$flag_24 && actor 1F within 20 units of pad D4
Flag 25 = actor 1F further than 180 units from pad D9

Actor 1F = robot
Pad D4 = corner between limit and drain
Pad D9 = near limo