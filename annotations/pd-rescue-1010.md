Keycard hot potato

Make 4 guards civilians
Wait for flag 7 (ending guards dead)

Seems like they try to give the keycard to the last guard who's alive, but they're all dead before the main part of the function runs so it's never reassigned.