Disguise timer

Wait until Jo has disguise or clothes guy is dead
Wait 3 seconds
"I don't have much time"
Start countdown timer (A = 3 mins, SA = 2m 15s, PA = 1m 30s)
Wait for timer to expire, flag 14 to be set, or door to be unlocked

Timer expired:
Activate alarm
"The enemy has discovered the disguise"
Set flag 17 (objective failed)

Flag 14 or door unlocked:
Cancel timer and exit