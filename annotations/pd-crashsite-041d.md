President running

For simplicity, this annotation ignores code relating to the president being injured.

The president can be in one of several statuses: InSight, RunningToJo or RunningToFinalPad.

Function ChooseStatus:

Stop moving
Restart timer
If in room 0x14 (same "room" as UFO), change status to RunningToFinalPad
If player in sight, change status to InSight
Else change status to RunningToJo

Status InSight:

If been InSight for 20 seconds, run ChooseStatus() again
If within 20 units of Jo:
- If in room 0x14 (same "room" as UFO), change status to RunningToFinalPad
- If player not in sight, change status to RunningToJo
If further than 20 units of Jo, change status to RunningToJo

Status RunningToJo:

Restart timer
Sample Jo's location and start running to it (if within 30 units of Jo, have hand up/forward while running)
While running:
- If in room 0x14 (same "room" as UFO), change status to RunningToFinalPad
- If within 10 units of Jo, ChooseStatus()
- If stopped moving (eg. reached the sampled location), ChooseStatus()
- If been following for 6 seconds, restart the RunningToJo status (ie. resample Jo's location)

Status RunningToFinalPad:

Run to pad
Wait until stopped
Set objective complete

