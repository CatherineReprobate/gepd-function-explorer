Hide final laser on overload

Seems like the function originally opened the laser/door and tried to keep it open, but was later changed to hide the laser/door instead.
Probably due to a single frame where it was closing and could block the player.