Invincible guard

Make actor invincible

Agent:
Wait until actor in sight of player or close to player, then make vulnerable

SA/PA:
Wait until actor in sight of player or close to player, or 3 seconds have passed, then make vulnerable

I think this is assigned to the two cloaked guards in the first room.