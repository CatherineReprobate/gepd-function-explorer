Spawn actor 04

The likelihood of a spawn depends on what weapon you have equipped, plus randomness.

........ | Silent | Klobb/GL/KF7 | Other
RNG 0-50 | No spawn | No spawn | Spawn
RNG 51-200 | No spawn | Spawn | Spawn
RNG 201-255 | Spawn | Spawn | Spawn