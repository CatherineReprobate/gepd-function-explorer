Console

If flags 11 or 12 are set, then when you activate the console nothing will happen.

When both flags are unset and the console is activated:
If flag 9 (exhaust bay open):
    If flag 17 (final checks cleared):
        "The exhaust bay is secured for shuttle launch!"
    Else
        Set flag 12
    End if
Else
    Set flag 11
End if